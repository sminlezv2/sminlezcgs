//
// Created by sminlez on 31.10.20.
//
#include "../Headers/gameselector.h"
#include "../Headers/mastermind.h"
void gameselector::runMasterMind(){
    std::string givenByUser;
    std::string toFind;
    std::string worbOutput;
    bool gamewon=false;
    bool gamelost=false;
    int timesfailed = 0;

    system("clear");
    std::cout << "Starting..." << std::endl;
    toFind = mastermind::randomizeString();
    std::cout << "Ready" << std::endl;
    //std::cout << toFind << std::endl;
    std::cout << "- means the letter isn't in the thing to find." << std::endl;
    std::cout << ". means the letter is in the thing to find but in the wrong place" << std::endl;
    std::cout << "X means the letter is in the right position" << std::endl;
    do{
        do {
            std::cout << "Guess the letters (5 letters from a to h)" << std::endl;
            std::cin >> givenByUser;
        }while(givenByUser.size() != 5);
    worbOutput = mastermind::worb(givenByUser, toFind);
    std::cout << worbOutput << std::endl;
    if(worbOutput=="xxxxx"){
        gamewon=true;
    }else if(timesfailed==14){
        gamelost=true;
    }else{
        std::cout << "not the right number" << std::endl;
        timesfailed++;
    }

    }while (gamewon==false && gamelost==false);
    std::cout << "The answer is : " << toFind << std::endl;
    if(gamewon==true){
        std::cout << "You won !" << std::endl;
    } else{
        std::cout << "You lost" << std::endl;
    }
}
